#
#  --- AJBSP ---
#
#  Makefile for Unixy system-wide install
#

PROGRAM=ajbsp

# prefix choices: /usr  /usr/local  /opt
PREFIX=/usr/local

OBJ_DIR=obj_linux

WARNINGS=-Wall -Wextra -Wshadow -Wno-unused-parameter
OPTIMISE=-O2 -fno-strict-aliasing -fno-exceptions -fno-rtti
STRIP_FLAGS=--strip-unneeded

# operating system choices: UNIX WIN32
OS=UNIX


#--- Internal stuff from here -----------------------------------

CXXFLAGS=$(OPTIMISE) $(WARNINGS) -D$(OS)  \
         -D_THREAD_SAFE -D_REENTRANT

LDFLAGS=-static-libgcc -static-libstdc++

LIBS=-lm -lz

DUMMY=$(OBJ_DIR)/zzdummy


#----- Object files ----------------------------------------------

OBJS = \
	$(OBJ_DIR)/bsp_level.o \
	$(OBJ_DIR)/bsp_node.o  \
	$(OBJ_DIR)/bsp_util.o  \
	$(OBJ_DIR)/lib_util.o  \
	$(OBJ_DIR)/lib_file.o  \
	$(OBJ_DIR)/main.o      \
	$(OBJ_DIR)/w_wad.o

$(OBJ_DIR)/%.o: src/%.cc
	$(CXX) $(CXXFLAGS) -o $@ -c $<


#----- Targets -----------------------------------------------

all: $(DUMMY) $(PROGRAM)

clean:
	rm -f $(PROGRAM) $(OBJ_DIR)/*.* core core.* ERRS

# this is used to create the OBJ_DIR directory
$(DUMMY):
	mkdir -p $(OBJ_DIR)
	@touch $@

$(PROGRAM): $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS) $(LIBS)

stripped: $(PROGRAM)
	strip $(STRIP_FLAGS) $(PROGRAM)

# TODO : install, uninstall

.PHONY: all clean stripped install uninstall

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
